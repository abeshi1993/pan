/* eslint-disable prettier/prettier */
import css from './header.module.css'

export default function Header(props) {
  return (
    <div className={css['nav-top']}>
      {/**<!---顶部logo-->**/}
      <div className={['max-w-7xl mx-auto sm:px-0 lg:px-3'].join(' ')}>
        <div className={css['logo-img']} style={{padding: '5px 0'}}>
          <img src='https://pp.myapp.com/ma_icon/0/icon_116071_1648016277/96' width={50} height={50} />
        </div>
        <div className={css['logo-title']}>网盘图床</div>
        {props.children}
      </div>
    </div>
  )
}
