/* eslint-disable prettier/prettier */
import { useRouter } from 'next/router'
import { useCallback, useEffect, useMemo, useState, useRef } from 'react'
import Link from 'next/link'
import dynamic from 'next/dynamic'


import _ from 'lodash';

import Header from '../containers/header';
import Loading from '../components/loading/index';

import { joinParams, getExtIcon } from '../utils'
import Toast from '../components/toast';
import ImgViewer from '../components/imgviewer';

import axios from 'axios'

window.Toast = Toast;
export default function User() {
    
    let [auth, setAuth] = useState(1)
    // 优化优先从localStorage中获取，然后再从服务端拉取更新
    let [user, setUser] = useState({
        netdisk_name: '-',
        avatar_url: 'https://dss0.bdstatic.com/7Ls0a8Sm1A5BphGlnYG/sys/portrait/item/netdisk.1.478ae50c.JoKKYkq1pE2suhjwNs_wiQ.jpg',
        contact: '-',
        expires: '-',
        uk: ''
    })
    
    useEffect(() => {
        const params = location.hash.slice(1);
        let p =  params.split('&').reduce((prev, next) => {
            let [k, v] = next.split('=')
            return Object.assign(prev, {[k]: v})
        }, {})
        const {access_token, error, expires_in} = p
        if (!access_token && !localStorage['jwt']) {
            location.href = 'https://openapi.baidu.com/oauth/2.0/authorize?response_type=token&client_id=OQRpOdysKaSlKIWYS8b2ix5DDD5RFc89&redirect_uri=https://pan-telanx.vercel.app/user&scope=basic,netdisk&state=STATE'
        } else {
            // 获取用户的信息，access_token或者从jwt中获取
            axios({
                url: '/api/baidu_login',
                params: {expires: expires_in, access_token},
                method: 'GET',
                headers: {
                    'Authorization': 'Bearer ' + localStorage['jwt']||''
                }
            }).then(res => {
                if (!res.data.code) {
                    setAuth(0)
                    Toast.info(res.data.msg)
                }
                // 本地更新jwt和其它用户信息
                if (res.data.jwt) {
                    localStorage['jwt'] = res.data.jwt
                }
                setUser({
                    netdisk_name: res.data.baidu_name,
                    avatar_url: res.data.avatar_url,
                    expires: res.data.expires,
                    uk: res.data.uk
                })
                setAuth(1)
            }).catch(err => {
                Toast.info('获取用户数据失败！')
            })
        }
        
    }, [])
    

    if (!auth) {
        return (<div>授权失败或登录已过期！<a href="/">返回首页</a> | <a href="https://openapi.baidu.com/oauth/2.0/authorize?response_type=token&client_id=OQRpOdysKaSlKIWYS8b2ix5DDD5RFc89&redirect_uri=https://pan-telanx.vercel.app/user&scope=basic,netdisk&state=STATE">重新授权</a></div>)
    }

    const redirectToMyPan = url => {location.replace(url)}
    return (<div>
    <Header />
    <main>
        <div className="max-w-7xl mx-auto  sm:px-0 lg:px-3">
            <h3>基本配置</h3>
            <hr/>
            
                <div className="line">
                    <div className="x4 text-center">
                        <img className="img-border border-green radius-circle mx-auto"
                            src={user.avatar_url} />
                        <b>{user.netdisk_name}</b>
                        
                    </div>
                    <div className="x8">
                        <form className="form form-x">
                            <div className="form-group">
                                <label className="label">主页地址</label>
                                <p className="field">
                                    <a href={'https://pan-telanx.vercel.app/'+user.uk||''}>
                                    https://pan-telanx.vercel.app/{user.uk}
                                    </a>
                                </p>
                            </div>
                            <div className="form-group">
                                <label className="label">有效期限</label>
                                <p className="field">
                                    截止{user.expires}
                                </p>
                            </div>

                            {/**<div className="form-group">
                                <label className="label">联系方式</label>
                                <p className="field">
                                    <input className="input form-control" defaultValue={user.contact}/>
                                </p>
                            </div>
                        **/}
                        
                        
                        <button className="button button-block bg-main" onClick={e => {e.preventDefault();redirectToMyPan('/'+user.uk)}}>进入我的网盘图床</button>
                        
                        <br/>
                        
                        </form>
                        
                            
                        
                        
                    </div>
                </div>
                
        
                <hr/>
        其它考虑功能:
            <ul>
                <li>上传限制等</li>
                <li>发送右键定时提醒更新token</li>
                <li>直接操作网盘增删改查</li>
            </ul>
        </div>
        
    </main>
</div>)
}