import { useEffect,useState } from "react"
import Link from "next/link"
import Header from "../containers/header"
import axios from 'axios'
import Toast from "../components/toast"

window.Toast = Toast
export default function Index() {
    let [users, setUsers] = useState([])
    // 获取列表数据
    useEffect(() => {
        axios.get('/api/users')
            .then(({data}) => {
                console.log(data)
                setUsers(data)
            })
            .catch(err => {
                Toast.error('获取数据失败！')
            })
    }, [])

    return (<div>
            <Header>
                <div style={{float: 'right', color: '#fff', lineHeight: '60px', padding: '0 10px'}}>
                    <a href='/user' className="button bg-sub">授权管理</a>
                    </div>
            </Header>
            <div className="px-0 py-2  md:px-5">
                <p>百度网盘没有进行防盗链处理，因此可以将百度网盘当免费图床使用。</p>
                <p>使用说明：点击右上角授权管理，一键开启网盘图床！</p>
            </div><hr />
            <ul className="px-2 py-2  xl:columns-5 lg:columns-4 md:columns-3 sm:columns-2">
            
            
            {users.map((file, idx) => {
              let next_dir =  '/' + file.uk
              let IconOrImg = <Link href={next_dir}>
                  {/**<div className="icon-folder-o" style={{fontSize:'100px', background:'url('+file.avatar_url+')'}}></div>**/}
                  
                      <img src={file.avatar_url} width="128" height="128" className="mx-auto img-border radius-circle" />
                  
                  
                  </Link>
              let hasExpired = false
              if (new Date(file.expires) < new Date()) {
                  hasExpired = true
              }
              return (<li style={{padding:'10px'}} className="link flex-auto mb-2" key={idx}>                        
                            <div className="text-center">
                                    {IconOrImg}                                    
                                    {file.netdisk_name}
                                    {
                                    hasExpired ? (<span class="tag bg-gray">过期</span>)
                                                : (<span class="tag bg-main">正常</span>)
                                    }
                                    
                                    </div>
                    </li>);
          })}
          
      </ul>
        </div>)
}