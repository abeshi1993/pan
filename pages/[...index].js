/* eslint-disable prettier/prettier */
import { useRouter } from 'next/router'
import { useCallback, useEffect, useMemo, useState } from 'react'
import Link from 'next/link'
import dynamic from 'next/dynamic'
import {list_data} from './api/mockdata.js'

import {useSWRInfinite} from 'swr'

import _ from 'lodash';

import Header from '../containers/header';
import Loading from '../components/loading/index';

import { joinParams, getExtIcon } from '../utils'
import Toast from '../components/toast';
import ImgViewer from '../components/imgviewer';

window.Toast = Toast;
const Upload = dynamic(
    () => import('../components/upload/upload.js'),
    { loading: () => (<p>正在加载中...</p>), ssr: false }
);

const DISK_API = '/api/disk'

var _size, _setSize, _isReachingEnd = false;
const PAGE_SIZE = 15;
const getKey = (dir, user) => (pageIndex, previousPageData) => {
    if (dir == '/[...index]') return null;   // 过滤掉刚开始进去页面
    if (previousPageData && !previousPageData.length) return null // reached the end
    return `${DISK_API}?type=list&user=${user}&size=${PAGE_SIZE}&folder=${dir}&page=${pageIndex+1}`                    // SWR key
  }
const fetcher = url => fetch(url).then(r => r.json())

export default function Index() {
    const router = useRouter()
    const paths = (router.query.index || []);
    // 开始进入页面的是/[...disk]
    const current_dir = router.asPath;
    const [loading, setLoading] = useState(0);
    const [loading_more, setLoading_more] = useState(0);
    const [canloadmore, setCanloadmore] = useState(1);
    const [page, setPage] = useState(0);
    const [upload_show, setUpload_show] = useState(0);
    const [isCreateDir, setCreateDir] = useState(false);
    const [inputDir, setInputDir] = useState('');
    const [showImgViewer, setImgViewerShow] = useState(0)
    const [current_pic, setCurrentPic] = useState(0)

    let replacePath = current_dir == '/disk' ? '/' : '';
    let dir = current_dir.replace(/^\/disk/, replacePath);
        
    let user=/\/(\d+)/.test(dir) && dir.match(/\/(\d+)/)[1] || ''
    dir = dir.replace(/\d+/, '')
    const {data, size, error, setSize} = useSWRInfinite(getKey(dir, user), fetcher, {
        shouldRetryOnError: false,
        revalidateOnFocus: false,
        fallbackData: [{a:1}]
    });
    _size = size;
    _setSize = setSize;
    _isReachingEnd = data?.[0]?.length == 0 || data && data[data.length - 1]?.length < PAGE_SIZE;

    useEffect(() => {
        if (error) {
            Toast.error('获取数据失败！')
        }
        if (!error && (!data || data && typeof data[size-1] == 'undefined')) {
            setLoading(1);
        } else {
            setLoading(0);
        }
    }, [size, data, error])
    
    
    const files = (data ? [].concat(...data) : []);
    const pics = files.filter(e =>
        e.url && /[jpg|png|jpeg|gif|bmp|webp]$/.test(e.file_name)
    ).map(e => e.url.replace(/c\d+_u\d+/, 'c4000_u2000'));
    const onImgViewerClose = e => {
        setImgViewerShow(0)
    }
    const handlePicClick = url => {
        setImgViewerShow(1)
        setCurrentPic(url)
    }
    const handleDownload = link => {
        // 复制文本
        let url = document.querySelector('#copy-text') || (function(){
            let txt = document.createElement('textarea');
            txt.setAttribute('id','copy-text')
            txt.setAttribute('style', 'width: 1;height:1;position: absolute; top: -100px')
            document.body.appendChild(txt);
            return txt
        })();
        
        url.value = location.host + link;
        url.select(); // 选择对象
        if(document.execCommand('Copy')) {
            Toast.info('已复制链接')
        } else {
            Toast.error('复制链接失败！')
        }
        url.blur()
    }
    const handleDelete = async (fs_id) => {
        setLoading(1);
        const params = {type: 'del'}
        let url = joinParams(DISK_API, params);
        try {
            let res = await fetch(url, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({fs_ids:[fs_id]})
            }).then(r => r.json());    
            Toast.info(res.msg)
        } catch(err) {
            Toast.error('系统繁忙！')
        } finally {
            setLoading(0)
        }
    }
    useEffect(() => {
        window.addEventListener('scroll', _.throttle(e => {
            if (loading) {return;}
            //if (window.pageYOffset + window.innerHeight > )
            // scrollTop 滚动条滚动时，距离顶部的距离
            var scrollTop = document.documentElement.scrollTop || document.body.scrollTop;
            // windowHeight 可视区的高度(在手机端网址栏消失的时候window.innerHeight才是准确的)
            var windowHeight = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
            // scrollHeight 滚动条的总高度
            var scrollHeight = document.documentElement.scrollHeight || document.body.scrollHeight;
            // 滚动条到底部的条件
            if (scrollTop + windowHeight + 1 > scrollHeight && !_isReachingEnd) {
                console.log('到达底部,_size', _size);
                _setSize(_size + 1);
            }
        }, 1000))
    }, [])

    
    // 完成滚动到底更新下一页，如果没有下一页，则停止更新数据
    
    const createDir = async path => {
        setCreateDir(!isCreateDir);
        if (!isCreateDir) {
            setInputDir('');
        }
    }
    const createDirReq = async () => {
        setLoading(1);
        let replacePath = '';
        if (current_dir == '/disk') replacePath = '/';
        const params = {type: 'create', folder: current_dir.replace(/^\/disk/, replacePath), isdir: 1, dir: inputDir};
        let url = joinParams(DISK_API, params);
        let res = await fetch(url,
            {
            method: 'GET',
            });
        if (res) {
            window.router = router;
            router.push(current_dir+'/'+inputDir);
            setLoading(0);
        }
        setCreateDir(false);
    }

    const uploadFile = () => {
        setUpload_show((upload_show+1)%2);
    }
    
    useEffect(() => {
        //setPage(0);
        if (/\[...disk\]/.test(current_dir)) {
            return;
        }
        let replacePath = '';
        
        if (current_dir == '/disk') {
            replacePath = '/';
        }
        if (current_dir.replace(/^\/disk/, replacePath) !== '/') {
            // 变动了size
            console.log('路由变动了')
            _setSize(1)
        }
    }, [current_dir])
    
    return (<div>
    <Header>
    <div style={{float: 'right', color: '#fff', lineHeight: '60px', padding: '0 10px'}}>
                    <button  className="button bg-sub" onClick={uploadFile}>{upload_show ? '关闭':'+'}上传图片</button>
                    </div>
    </Header>
    {/**<!---上传区域-->**/}
    {!!upload_show && <Upload path={current_dir}/>}
    <main>
    {/**loading画面 */}
    {!!loading && <Loading />}
    <div className="max-w-7xl mx-auto py-2 sm:px-0 lg:px-3">
      {/**<!-- Replace with your content -->**/}
      <div className='path'>
          <ul className="bread text-big" style={{margin: 0,padding: 0}}>
            <li><Link className='link' key='/' href='/'><a className="icon-home">网盘</a></Link></li>
            
            {paths.map((path, idx, arr) => {
                
                let relative_path = '/' + arr.slice(0, idx+1).join('/');
                if (path == '') {
                    return <span className='link' key={path}><Link href={relative_path}>全部文件</Link></span>;
                }
                return (<li><a className="link" key={path}>
                    <Link href={relative_path}>{path}</Link>
                        </a></li>)
            })}
            </ul>
            {
                isCreateDir
                    ? <span>/ <input type="input" value={inputDir} onChange={e=>setInputDir(e.target.value)}/><button className="button button-small bg-sub" onClick={createDirReq}>确定</button></span>
                    : null
            }
            <span className="float-right">
                {/**<button className="button border-main sm:button-small" onClick={createDir}>{isCreateDir?'取消':'新建目录'}</button>**/}
                
            
            </span>
        </div>
        <hr />

        <ul className="px-2 py-2  xl:columns-5 lg:columns-4 md:columns-3 sm:columns-2 sm:px-0">
          {files.map((file, idx) => {
              let next_dir = current_dir + '/' + file.file_name;

              let IconOrImg;
              if (file.is_dir) {
                  IconOrImg = <Link href={next_dir}><div className="icon-folder-o" style={{fontSize:'100px'}}></div></Link>
              } else {
                  if(file.url && /[jpg|png|jpeg|gif|bmp|webp]$/.test(file.file_name)) {
                    let file_url = file.url.replace(/c\d+_u\d+/, 'c600_u800');
                    IconOrImg = (<div className="flex justify-center img-container">
                        <div className="absolute w-40 justify-around img-opera"
                            style={{background: 'rgba(88,88,88, .3)', color: '#fff'}}>
                            <i title="预览" className="icon" onClick={e => handlePicClick(file.url.replace(/c\d+_u\d+/, 'c4000_u2000'))}>
                                <svg  xmlns="http://www.w3.org/2000/svg"  className="w-4 h-4" viewBox="0 0 20 20" fill="currentColor">
                                <path d="M10 12a2 2 0 100-4 2 2 0 000 4z" />
                                <path fillRule="evenodd" d="M.458 10C1.732 5.943 5.522 3 10 3s8.268 2.943 9.542 7c-1.274 4.057-5.064 7-9.542 7S1.732 14.057.458 10zM14 10a4 4 0 11-8 0 4 4 0 018 0z" clipRule="evenodd" />
                                </svg>
                            </i>
                            <i title="直链" className="icon" onClick={e=>handleDownload('/api/file/'+file.uk+'/'+file.fs_id+'/500_500')}>
                            <svg xmlns="http://www.w3.org/2000/svg" className="h-4 w-4" viewBox="0 0 20 20" fill="currentColor">
                            <path fillRule="evenodd" d="M12.586 4.586a2 2 0 112.828 2.828l-3 3a2 2 0 01-2.828 0 1 1 0 00-1.414 1.414 4 4 0 005.656 0l3-3a4 4 0 00-5.656-5.656l-1.5 1.5a1 1 0 101.414 1.414l1.5-1.5zm-5 5a2 2 0 012.828 0 1 1 0 101.414-1.414 4 4 0 00-5.656 0l-3 3a4 4 0 105.656 5.656l1.5-1.5a1 1 0 10-1.414-1.414l-1.5 1.5a2 2 0 11-2.828-2.828l3-3z" clipRule="evenodd" />
                            </svg>
                            </i>
                            {/**
                             <i title="删除" className="icon" onClick={e=>handleDelete(file.fs_id)}>
                                <svg xmlns="http://www.w3.org/2000/svg" className="w-4 h-4"  viewBox="0 0 20 20" fill="currentColor">
                                <path fillRule="evenodd" d="M9 2a1 1 0 00-.894.553L7.382 4H4a1 1 0 000 2v10a2 2 0 002 2h8a2 2 0 002-2V6a1 1 0 100-2h-3.382l-.724-1.447A1 1 0 0011 2H9zM7 8a1 1 0 012 0v6a1 1 0 11-2 0V8zm5-1a1 1 0 00-1 1v6a1 1 0 102 0V8a1 1 0 00-1-1z" clipRule="evenodd" />
                                </svg>
                             </i>
                            **/}
                        </div>
                        <img src={file_url} />
                    </div>)
                  } else {
                    let ext = file.file_name.split('.').pop();
                    IconOrImg = <div className={getExtIcon(ext)} style={{fontSize:'100px'}}></div>
                  }
              }
              return (<li style={{padding:'10px'}} className="link flex-auto mb-2" key={idx}>                        
                            <span>
                                    {IconOrImg}                                    
                                    <div className="file-name overflow-hidden overflow-ellipsis">{file.file_name}</div>
                                </span>
                            
                    </li>);
          })}
          
      </ul>
      {_isReachingEnd && <p className="text-center">-END-</p>}
      {/**<!-- /End replace -->**/}
    </div>
  </main>
  <ImgViewer show={showImgViewer} lists={pics} current={current_pic} onClose={onImgViewerClose}/>
</div>)
}