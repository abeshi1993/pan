import multiparty from 'multiparty'
import conn from './utils/conn'
import Pan from './utils/pan'

export default async function Upload(req, res) {
  const form = new multiparty.Form()
  form.parse(req, async (err, fields, files) => {
    if (err) {
      return res.status(500).end('500 请求错误！')
    }
    let [folder] = fields.folder
    if (!/^\/\d+/.test(folder)) {
      return res.status(500).json({
        code: 0,
        msg: '禁止上传！'
      })
    }
    let [replaceStr, uk] = folder.match(/^\/(\d+)/)
    folder = folder.replace(/^\/\d+/, '/')

    try {
      let [re, _] = await conn.query(`select * from pan_user where uk='${uk}'`)
      if (!re.length) {
        return res.status(500).json({
          code: 0,
          msg: '用户不存在！'
        })
      }

      let access_token = re[0].access_token;
      // 从folder中解析出access_token,未完待续...
      const { path, originalFilename } = files.file[0]
      //const file_stream = fs.readFileSync(path);

      const file_name = originalFilename
      const pan = new Pan(access_token)
      let fs_id, file_size
      
      const [res2, err2] = await pan.uploadFile(file_name, path, folder)
      if (err2) {
        return res.status(500).json(err2)
      } else {
        fs_id = res2.fs_id
        file_size = res2.size
      }
      return res.json({
        code: 1,
        msg: '上传成功！'
      })
    } catch (err) {
      console.log(err)
      return res.status(500).end(JSON.stringify(err))
    }
  })
}

// 关闭内置的bodyParser让multiparty解析
export const config = {
  api: {
    bodyParser: false
  }
}
