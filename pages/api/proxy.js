import fs from 'fs'
import axios from 'axios'

export default async (req, res) => {
  const {
    query: { url }
  } = req

  axios({
    url: decodeURI(url),
    headers: {
      'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
      'Accept-Encoding': 'gzip, deflate, br',
      'Referrer': url,
      'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.74 Safari/537.36 Edg/99.0.1150.46',
      'Sec-Fetch-Dest': 'image',
    'Sec-Fetch-Mode': 'no-cors',
    'Sec-Fetch-Site': 'same-origin'
    }
  })
    .then((r) => {
      
      for (var i in r) {
          if (r.headers[i])
        res.setHeader(i, r.headers[i])
      }
      console.log(r.config)
      res.send(r.data)
    })
    .catch((err) => {
      console.log(err)
    })
}
