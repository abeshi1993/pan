const mysql = require('mysql2')
const conn = mysql.createConnection(process.env.DATABASE_URL)
export default conn.promise()
