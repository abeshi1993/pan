import axios from 'axios'
import fs from 'fs'
import mime from 'mime'

const joinParams = (query, noprefix = 0) =>
  Object.keys(query)
    .reduce(
      (prev, k) => {
        let v = query[k]
        if (typeof v == 'object') {
          v = JSON.stringify(v)
        }
        return prev + `${k}=${v}&`
      },
      noprefix ? '' : '?'
    )
    .replace(/&$/, '')

// 构造multipart/form-data格式
// buildMultiForm(fields = [{name: '', value: }], files = [{name:'file', value: '', path: ''}])
// return [form, contentType]
const buildMultiForm = (fields, files) => {
  const boundary = '------multipartformboundary1642088724247'
  const content_type = 'multipart/form-data; boundary=' + boundary
  const fields_data = fields
    .map((field) => {
      return (
        `--${boundary}\r\n` +
        `Content-Disposition: form-data; name="${field.name}"\r\n` +
        '\r\n' +
        `${field[value]}` +
        `\r\n`
      )
    })
    .join('')
  const files_data = files.map((file) => {
    let mime_type = mime.getType(file.path)
    let file_data = fs.readFileSync(file.path)
    return Buffer.concat([
      Buffer.from(
        `--${boundary}\r\n` +
          `Content-Disposition: form-data; name="${file.name}"; filename="${file.value}"\r\n` +
          `Content-Type: ${mime_type}\r\n` +
          '\r\n'
      ),
      file_data,
      Buffer.from('\r\n')
    ])
  })
  const end_data = `--${boundary}--\r\n\r\n`
  const form_data = Buffer.concat([Buffer.from(fields_data), Buffer.concat(files_data), Buffer.from(end_data)])
  return [form_data, content_type]
}

// 百度网盘API
class PanError extends Error {
  constructor({ code, err, msg }) {
    super()
    this.code = code
    this.err = err
    this.msg = msg
  }
  toString() {
    const { code, err, msg } = this
    return `出错了：code:${code}  msg:${msg}`
  }
  toJSON() {
    const { code, err, msg } = this
    return { code, err, msg }
  }
}

class Pan {
  // 获取access_token
  constructor(access_token) {
    if (!access_token) {
      throw new Error('请初始化access_token')
    }
    this.access_token = access_token
  }

  refreshAccesstoken(access_token) {
    this.access_token = access_token
  }

  // 获取用户基本信息
  getUserInfo() {
    const API_USER_INFO = 'https://pan.baidu.com/rest/2.0/xpan/nas'
    const query = {
      method: 'uinfo',
      access_token: this.access_token
    }
    return axios.get(API_USER_INFO + joinParams(query))
  }

  // 获取指定用户指定路径文件列表
  getUserFiles(dir, start, limit) {
    const API_FILE_LIST = 'https://pan.baidu.com/rest/2.0/xpan/file'
    const query = {
      method: 'list',
      access_token: this.access_token,
      dir,
      start,
      limit,
      web: 'web',
      show_empty: 0
    }
    return axios.get(API_FILE_LIST + joinParams(query))
  }

  // 获取uploadid
  precreateFile(cloud_path, file_size) {
    const API_FILE_PRECREATE = 'https://pan.baidu.com/rest/2.0/xpan/file'
    const query = {
      method: 'precreate',
      access_token: this.access_token
    }
    const body = {
      path: cloud_path, // /apps/appName/filename
      size: file_size,
      isdir: 0, // 0文件 1目录
      autoinit: 1,
      rtype: 1, // optional
      //"uploadid" => "",	// optional
      block_list: '["0"]'
    }
    const url = API_FILE_PRECREATE + joinParams(query)
    console.log(`-----------预请求------------`)
    console.log(`url: ${url}`)
    console.log(joinParams(body, 1))
    return axios({ url, method: 'post', data: joinParams(body, 1) })
  }
  postFile(cloud_path, localpath, uploadid) {
    const API_FILE_UPLOAD = 'https://d.pcs.baidu.com/rest/2.0/pcs/superfile2'
    try {
      var query = {
        method: 'upload',
        access_token: this.access_token,
        type: 'tmpfile',
        path: cloud_path,
        uploadid: uploadid,
        partseq: 0
      }
      const url = API_FILE_UPLOAD + joinParams(query)
      console.log(url)
      const filename = cloud_path.split('/').pop()
      const [multiForm, contentType] = buildMultiForm([], [{ name: 'file', value: filename, path: localpath }])
      console.log('-----------上传文件---------------')
      console.log(`url: ${url}`)
      console.log(Buffer.byteLength(multiForm))
      console.log(contentType)
      return axios({
        url,
        method: 'post',
        headers: {
          'Content-Type': contentType
          //'Content-Length': Buffer.byteLength(multiForm)
        },
        data: multiForm
      })
    } catch (err) {
      console.log(err)
    }
  }

  createFile({ filename, filesize, filemd5, uploadid }) {
    const API_FILE_CREATE = 'https://pan.baidu.com/rest/2.0/xpan/file'
    const query = {
      method: 'create',
      access_token: this.access_token
    }
    const url = API_FILE_CREATE + joinParams(query)
    const cf_payload = {
      path: filename,
      size: filesize,
      rtype: '1',
      isdir: '0',
      uploadid: uploadid,
      block_list: `["${filemd5}"]`
    }
    console.log('----------创建文件-------------')
    console.log(`url: ${url}`)
    console.log(joinParams(cf_payload, 1))
    return axios({
      url,
      headers: {
        'User-Agent': 'pan.baidu.com'
      },
      method: 'post',
      data: joinParams(cf_payload, 1)
    })
  }

  async uploadFile(filename, path, dir = '') {
    const filesize = fs.statSync(path).size // 默认utf8编码
    const cloud_path = dir + '/' + filename
    // 获取uploadid
    let uploadid, filemd5
    try {
      const { data } = await this.precreateFile(cloud_path, filesize)
      if (data.errno != 0) {
        return [void 0, new PanError({ code: 0, err: data, msg: data.msg || '网盘接口错误' })]
      }
      uploadid = data.uploadid
      console.log(data)
    } catch (err) {
      return [void 0, new PanError({ code: 0, err, msg: '获取uploadid失败' })]
    }
    // 上传文件流
    try {
      const { data } = await this.postFile(cloud_path, path, uploadid)
      if (!data || !data.md5) {
        return [void 0, new PanError({ code: 0, err: data, msg: data.msg || '上传文件失败' })]
      }
      filemd5 = data.md5
      console.log(data)
    } catch (err) {
      console.log(err)
      return new PanError({ code: 0, err, resp: '上传文件失败！' })
    }
    // 创建文件获取fs_id
    try {
      const { data } = await this.createFile({ filename: cloud_path, filesize, filemd5, uploadid })
      if (!data || !data.fs_id) {
        return [void 0, new PanError({ code: 0, err: data, msg: data.msg || '云端创建文件失败' })]
      }
      return [data, void 0]
    } catch (err) {
      console.log(err)
      return new PanError({ code: 0, err, resp: '创建文件失败！' })
    }
  }
  // 获取fs_id的下载链接
  async getMultimedias(fs_ids) {
    const API_LIST_MULTIMEDIA = 'http://pan.baidu.com/rest/2.0/xpan/multimedia'
    const query = {
      access_token: this.access_token,
      method: 'filemetas',
      fsids: '[' + fs_ids.join(',') + ']',
      thumb: 1,
      dlink: 1,
      extra: 1
    }
    const url = API_LIST_MULTIMEDIA + joinParams(query)
    try {
      const { data } = await axios.get(url)
      if (!data || data.errno != 0) {
        return [void 0, new PanError({ code: 0, msg: '获取网盘数据失败！' })]
      }
      const res = data.list.map((row) => {
        return {
          fs_id: row.fs_id,
          dlink: row.dlink,
          url: row?.thumbs?.url4 || row?.thumbs?.url3
        }
      })
      return [res, void 0]
    } catch (err) {
      console.log(err)
      return [void 0, new PanError({ code: 0, msg: '500系统错误！' })]
    }
  }
  /* 删除文件
  @params fs_ids ['122121', '122132']
  @return Promise
  */
  async del(fs_ids) {
    // 获取文件路径
  }
  async manage({type, filelist}) {
    // filelist传入的是fs_id还需要进行转化
    const API_FILE_MANAGE = 'https://pan.baidu.com/rest/2.0/xpan/file'
    const access_token = this.access_token
    let opera
    switch(type) {
      case 'del': opera = 'delete'
        break
      case 'copy': opera = 'copy'
        break
      case 'move': opera = 'mover'
        break
      case 'rename': opera = 'rename'
        break
      default:
        opera = ''
    }
    /**filelist参数
     * delete: [文件路径1, 文件路径]
     * copy/move: [{"path":"/测试目录/123456.docx","dest":"/测试目录/abc","newname":"11223.docx","ondup":"fail"}]
     * rename: [{path":"/测试目录/123456.docx","newname":test.docx"}]
     */
    const params = {
      async: 2, // 0同步 1自适应 2异步
      filelist,
      ondup: 'newcopy'  //fail失败 newcopy重命名 overwrite覆写 skip跳过
    }
    const query = {method: 'filemanager', opera}
    const url = joinParams(API_FILE_MANAGE, query)
    return axios({
      url,
      headers: {
        'User-Agent': 'pan.baidu.com'
      },
      method: 'post',
      data: joinParams(params, 1)
    })
  }
}
export default Pan

/**上传文件md5不一样，说明问题出在buffer拼接上 */
