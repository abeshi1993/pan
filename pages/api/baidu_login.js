//login_success#expires_in=86400&access_token=1.79b233866fb1640&session_secret=1c07332&session_key=9XY&scope=basic
//oauth_redirect#error=access_denied&error_description=user%20didnot%20allow%20your%20authorization%20request&state=xxx
// 获取登录成功的access_token，expires_in即可

import Pan from './utils/pan'
import conn from './utils/conn'
// 回调地址 https://pan-telanx.vercel.app/api/baidu_login

async function createOrUpdateUser({
  access_token,
  uk,
  baidu_name,
  avatar_url,
  expires,
  contact}) {
    // 先查询存不存在，如果存在的话就根据uk来更新，不存在就新建
    try {
      let [res, _] = await conn.query(`select * from pan_user where uk='${uk}'`)
      if (expires) {
        expires = new Date(new Date(new Date().toGMTString()).getTime() + 8 * 3600 * 1000 + parseInt(expires)*1000)
        .toJSON()
        .replace('T', ' ')
        .replace(/\.\d+Z/, '')
      }
      if (!res.length) {
        // 不存在则新插入一份
        
        let sql = `
        insert into pan_user(id, uk, access_token, netdisk_name, avatar_url, expires, contact) values(
          NULL, '${uk}','${access_token}', '${baidu_name}','${avatar_url}', '${expires}', '${contact}'
        )
        `
        console.log(sql)
        let rr = await conn.query(sql)
        console.log(rr)
      } else {
        // 更新数据
        console.log('已经存在uk更新')
        const columns = {
          access_token,
          netdisk_name: baidu_name,
          avatar_url,
          contact,
          expires
        }
        let update_columns = Object.keys(columns).filter(k => columns[k] && columns[k] !== '');
        let update_sql = update_columns.map(e => {
          return e + '=\'' + columns[e] + '\''
        }).join(',');
        let sql = `
          update pan_user set ${update_sql}
          where uk='${uk}'
        `
        console.log('已更新', sql)
        let rr = await conn.query(sql)
      }
    } catch(err) {
      console.log(err)
      conn.destroy()
    }
  }
export default async function handler(req, res) {
  
  // 直接注册到数据库
  // 如果有error就是失败
  // 对access_token进行校验
  const AUTH_EXPIRETIME = 30 * 24 * 3600;
  let {access_token, expires = AUTH_EXPIRETIME} = req.query;
  let jwt = req.headers.authorization.slice(7);
  // 优先对access_token进行解析，如果失败就错误，不存在就解析jwt
  
  
  try {
    const pan = new Pan(access_token||jwt)
    const re = await pan.getUserInfo()
    if (re.data.errno != 0) {
      return res.json({code: 0, msg: 'errno:'+re.data.errno})
    } else {
      // 成功就去新建或者更新pan_user信息里的access_token数据
      let {avatar_url, uk, baidu_name} = re.data;
      createOrUpdateUser({
        access_token: access_token||jwt,
        uk,
        baidu_name,
        avatar_url,
        expires
      })
      console.log(re.data)
      expires = new Date(new Date(new Date().toGMTString()).getTime() + 8 * 3600 * 1000 + parseInt(expires)*1000)
        .toJSON()
        .replace('T', ' ')
        .replace(/\.\d+Z/, '')
      return res.json(
        Object.assign({
          code: 1,        
          jwt: access_token,
          expires
        }, re.data)
      )
    }
    // 成功的话就将数据注入到数据库中，然后序列化到jwt的cookie
    return res.json({code: 1, access_token, jwt})
  } catch(err) {
    console.log(err)
    res.json({
      code: 0,
      msg: '非法access_token'
    })
  }
}
