import conn from './utils/conn'

/*<th>uk百度的唯一id</th>
                    <th>access_token访问token</th>
                    <th>netdisk_name网盘昵称</th>
                    <th>avata_url图标</th>
                    <th>expire_time过期时间</th>
                    <th>contact联系方式</th>
                    <th>ext其它扩展字段放这里</th>*/
export default async (req, res) => {
  try {
  let re = await conn.query(`
  CREATE TABLE pan_user (
    id int NOT NULL AUTO_INCREMENT,
    uk text DEFAULT NULL,
    access_token text NOT NULL,
    netdisk_name text,
    avatar_url text,
    expires datetime DEFAULT NULL,
    contact text,
    ext text,
    PRIMARY KEY (id)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
  `)
  res.end(JSON.stringify(re))
  } catch(err) {
    res.end(JSON.stringify(err))
  }
}