# 百度网盘图床

### 介绍
Vercel+百度网盘 = 免费的图床
图片保存在自己的百度网盘，不怕丢失！

图片直链URL `/api/file/[uk]/[fs_id]/[size]`
![猫和狗](https://pan-telanx.vercel.app/api/file/2227021265/1012011064537031/500_500)