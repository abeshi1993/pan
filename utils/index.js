export const joinParams = (url, params, noprefix = 0) =>
  Object.keys(params).reduce((prev, k) => {
    let v = params[k]
    v = typeof v == 'object' ? JSON.stringify(v) : v
    return `${prev}${k}=${v}&`
    }, url + (noprefix ? '&' : '?'))
    .replace(/&$/, '')

export const getExtIcon = (filename) => {
  if (/[(mp3)|(wma)|(wav)|(midi)|(ape)|(flac)|(ogg)|(acc)]$/.test(filename)) {
    return 'icon-file-audio-o'
  }
  if (/[(jpg)|(png)|(jpeg)|(gif)|(bmp)|(webp)]$/.test(filename)) {
    return 'icon-picture-o'
  }
  if (/[(mp4)|(flv)|(ts)|(mpeg)|(avi)|(rmvb)]$/.test(filename)) {
    return 'icon-file-video-o'
  }
  if (/[(doc)|(docx)]$/.test(filename)) {
    return 'icon-file-word-o'
  }
  if (/[(ppt)]$/.test(filename)) {
    return 'icon-file-ppt-o'
  }
  if (/[(pdf)]$/.test(filename)) {
    return 'icon-file-pdf-o'
  }
  if (/[(txt)]$/.test(filename)) {
    return 'icon-file-text-o'
  }

  return 'icon-file-o'
}
