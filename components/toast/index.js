// 直接插入到DOM中的
import { createRef, forwardRef } from 'react'
import ReactDOM from 'react-dom'
import Toast from './Toast'

let notification,
  configs = {
    time: 2000,
    showMask: false,
    position: 'center'
  },
  noticeRef = createRef()

const createNotification = () => {
  if (typeof window == 'undefined' || !'document' in window) {
    return false
  }
  const div = document.createElement('div')
  document.body.appendChild(div)

  let RefToast = forwardRef(Toast)
  ReactDOM.render(<RefToast ref={noticeRef} {...configs} />, div)
  return {
    destory() {
      ReactDOM.unmountComponentAtNode(div)
      document.body.removeChild(div)
    },
    clearAll() {
      noticeRef.current.setNotices([])
    }
  }
}

notification = notification || createNotification()

let notices = []
export default {
  setPosition(p) {
    configs.position = p
    return this
  },
  setTime(t) {
    configs.time = t
    return this
  },
  warn(text) {
    this.show({ text, type: 'warn' })
    return this
  },
  error(text) {
    this.show({ text, type: 'error' })
    return this
  },
  info(text) {
    this.show({ text, type: 'info' })
    return this
  },
  show(props) {
    const noticeConfig = { ...configs, ...props, t: new Date().getTime() }
    notices.push(noticeConfig)
    noticeRef.current.setNotices([...notices])
    setTimeout(() => {
      // 移除当前节点
      let i = 0
      while (noticeConfig.t !== notices[i].t) {
        i++
        if (i > notices.length) {
          break
        }
      }
      notices.splice(i, 1)
      noticeRef.current.setNotices([...notices])
    }, noticeConfig.time)
    return this
  }
}
